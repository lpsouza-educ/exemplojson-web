﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace ExemploJSON
{
    class Program
    {
        static void Main(string[] args)
        {
            WebClient client = new WebClient();
            string jsonString = client.DownloadString("https://jsonplaceholder.typicode.com/todos/");
            JArray json = JArray.Parse(jsonString);
            List<Todo> todos = json.ToObject<List<Todo>>();

            foreach (Todo todo in todos)
            {
                if (todo.completed)
                {
                    Console.WriteLine(string.Format("[X] {0}", todo.title));
                }
                else{
                    Console.WriteLine(string.Format("[ ] {0}", todo.title));
                }
            }

        }
    }
}
